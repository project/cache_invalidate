<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Specifies elements to invalidate its cache on certain events.
 *
 * @see _cache_invalidate_get_definitions()
 *
 * @return array
 *   An array of the elements definitions.
 */
function hook_cache_invalidate_info() {
  return array(
    'view_machine_name' => array(
      'type' => 'view',
      'name' => 'machine_name',
      'triggers' => array(
        'node_update' => array(
          'page',
          'article',
        ),
        'node_insert',
      ),
    ),
  );
}
