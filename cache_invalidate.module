<?php

/**
 * @file
 * Allows to clear entities cache based on different events.
 */

/**
 * Helper function get definitions provided by modules.
 */
function _cache_invalidate_get_definitions() {

  $data = &drupal_static(__FUNCTION__, NULL);
  if ($data !== NULL) {
    return $data;
  }
  $data = array();
  foreach (module_implements('cache_invalidate_info') as $module) {
    $result = call_user_func($module . '_cache_invalidate_info');
    if (!isset($result) || !is_array($result)) {
      continue;
    }
    $data += $result;
  }
  return $data;
}

/**
 * Helper function get definitions filtering by trigger.
 *
 * @param string $trigger
 *   The trigger name.
 * @param string $type
 *   The subtype filter.
 *
 * @return array
 *   The filtered definitions.
 */
function _cache_invalidate_get_definitions_by_trigger($trigger, $type = 'no_type') {
  $data = &drupal_static(__FUNCTION__, array());
  if (!isset($data[$trigger])) {
    $data[$trigger] = array();
  }
  if (isset($data[$trigger][$type])) {
    return $data[$trigger][$type];
  }
  $data = _cache_invalidate_get_definitions();
  foreach ($data as $key => $definition) {
    foreach ($definition['triggers'] as $trigger_key => $trigger_value) {
      if (is_array($trigger_value)) {
        if ($trigger_key != $trigger) {
          // Trigger not matching, continue.
          continue;
        }
        if ($type !== 'no_type' && !in_array($type, $trigger_value)) {
          // Type not matching, continue.
          continue;
        }
      }
      else {
        if ($trigger_value != $trigger) {
          // Trigger not matching, continue.
          continue;
        }
      }
      // Everything matches, add the trigger and go to next definition.
      $data[$trigger][$type][] = $definition;
      break;
    }
  }

  if (!isset($data[$trigger][$type])) {
    $data[$trigger][$type] = array();
  }

  return $data[$trigger][$type];
}

/**
 * Clears the cache for a definition.
 *
 * @param array $definition
 *   The definition array.
 *
 * @return bool
 *   Returns true if the definition was processed successfully.
 */
function cache_invalidate_clear_definition($definition) {
  switch ($definition['type']) {
    case 'view':
      cache_clear_all($definition['name'] . ':', 'cache_views', TRUE);
      cache_clear_all($definition['name'] . ':', 'cache_views_data', TRUE);
      return TRUE;
  }
  return FALSE;
}

/**
 * Process node triggers.
 *
 * @param object $node
 *   The node to process.
 * @param string $trigger
 *   The trigger name.
 */
function _cache_invalidate_process_node($node, $trigger) {
  $definitions = _cache_invalidate_get_definitions_by_trigger($trigger, $node->type);
  // Don't continue if we don't have any matching definition.
  if (empty($definitions)) {
    return;
  }
  foreach ($definitions as $definition) {
    cache_invalidate_clear_definition($definition);
  }
}

/**
 * Process node_queue triggers.
 */
function _cache_invalidate_process_subqueue($trigger, $sqid, $nid = NULL) {
  $subqueue = nodequeue_load_subqueue($sqid);
  if ($subqueue === NULL) {
    return NULL;
  }
  $nodequeue = nodequeue_load($subqueue->qid);
  if ($nodequeue === NULL) {
    return NULL;
  }
  $definitions = _cache_invalidate_get_definitions_by_trigger('nodequeue_' . $trigger, $nodequeue->name);
  if (empty($definitions)) {
    return NULL;
  }
  foreach ($definitions as $definition) {
    cache_invalidate_clear_definition($definition);
  }
}

/**
 * Process user triggers.
 *
 * @param string $trigger
 *   The trigger name.
 * @param array $edit
 *   The account array to process.
 * @param object $account
 *   The account object to process.
 * @param string $category
 *   The account category to process.
 */
function _cache_invalidate_process_user($trigger, &$edit, $account, $category) {
  $definitions = _cache_invalidate_get_definitions_by_trigger($trigger);
  // Don't continue if we don't have any matching definition.
  if (empty($definitions)) {
    return;
  }
  foreach ($definitions as $definition) {
    cache_invalidate_clear_definition($definition);
  }
}

/**
 * Implements hook_node_insert().
 */
function cache_invalidate_node_insert($node) {
  _cache_invalidate_process_node($node, 'node_insert');
}

/**
 * Implements hook_node_update().
 */
function cache_invalidate_node_update($node) {
  _cache_invalidate_process_node($node, 'node_update');
}

/**
 * Implements hook_node_delete().
 */
function cache_invalidate_node_delete($node) {
  _cache_invalidate_process_node($node, 'node_delete');
}

/**
 * Implements hook_nodequeue_swap().
 */
function cache_invalidate_nodequeue_swap($sqid, $nid) {
  _cache_invalidate_process_subqueue('swap', $sqid, $nid);
}

/**
 * Implements hook_nodequeue_add().
 */
function cache_invalidate_nodequeue_add($sqid, $nid) {
  _cache_invalidate_process_subqueue('add', $sqid, $nid);
}

/**
 * Implements hook_nodequeue_remove().
 */
function cache_invalidate_nodequeue_remove($sqid, $nid) {
  _cache_invalidate_process_subqueue('remove', $sqid, $nid);
}

/**
 * Implements hook_nodequeue_save_subqueue_order_alter().
 */
function cache_invalidate_nodequeue_save_subqueue_order_alter($sqid, $nodes) {
  _cache_invalidate_process_subqueue('order', $sqid);
}

/**
 * Implements hook_user_insert().
 */
function cache_invalidate_user_insert(&$edit, $account, $category) {
  _cache_invalidate_process_user('user_insert', $edit, $account, $category);
}

/**
 * Implements hook_user_update().
 */
function cache_invalidate_user_update(&$edit, $account, $category) {
  _cache_invalidate_process_user('user_update', $edit, $account, $category);
}

/**
 * Implements hook_user_delete().
 */
function cache_invalidate_user_delete($account) {
  // We can only pass variables by reference, so we set this to NULL here.
  $edit = NULL;
  _cache_invalidate_process_user('user_delete', $edit, $account, NULL);
}
